So you want to contribute? Great!

There are two ways to contribute:

- Fork it, modify it, and create a merge request.
- Create an issue.

Please include the following:
- title
- link to primary/secondary source
- brief description.

Conduct:
- be respectful, use common sense
- Intolerance is [not tolerated](https://en.wikipedia.org/wiki/Paradox_of_tolerance)
